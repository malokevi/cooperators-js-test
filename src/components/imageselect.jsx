import React, { Component } from 'react';

class ImageSelect extends Component {
    render() { 
        const {imageTypes, imageGenerate, handleChange} = this.props;

        return ( 
            <React.Fragment>
                <label>Select Filetype</label>
                <select onChange={this.props.handleChange} className="dropDown" ref="fileType" name="fileType" id="fileTypeId">
                    <option selected="selected" disabled="disabled">{ ' ' }</option>
                { this.props.imageTypes.map(imageType => 
                <option className="dropdown-item" key={ imageType.id } value={ imageType.value } imageType={ imageType.value  }> 
                    { imageType.value }
                </option>
                )}
                </select>
            </React.Fragment>
         );
    }
}
 
export default ImageSelect;