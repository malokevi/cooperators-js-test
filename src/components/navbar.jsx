import React, { Component } from 'react';
import './../App.css';

//stateless functional component
const Navbar = () => {
    return ( 
        <nav className="navbar navbar-light blueNav">
          <a className="navbar-brand navText" href="#">
            Navbar{" "}
          </a>

          <span>Created by Kevin Malone</span>
        </nav>
     );
}
 
export default Navbar;