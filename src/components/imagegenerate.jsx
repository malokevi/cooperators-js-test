import React, { Component } from 'react';

class ImageGenerate extends Component {
    render() { 
        const {imageTypes, imageGenerate} = this.props;

        return ( 
            <React.Fragment>
                <div className="loaderImage">
                    <div class="lds-circle"><div></div></div>
                </div>

                <div id="imageDiv" class="imageDiv"></div>
            </React.Fragment>
         );
    }
}
 
export default ImageGenerate;