import React, { Component } from 'react';
import Navbar from './components/navbar';
import ImageSelect from './components/imageselect';
import ImageGenerate from './components/imagegenerate';
import './App.css';
import $ from 'jquery';

class App extends Component {
    state = { 
      imageTypes : [
          {id: 1, value: 'jpeg'},
          {id: 2, value: 'png' },
          {id: 3, value: 'svg' },
          {id: 4, value: 'invalid' }
      ],
      value : ' ',
      description : 'Create a responsive web page using any front-end technology (js/jQuery/Angularjs/React/Bootstrap/Sass) which has a simple form with one dropdown option (Image type) and description text on top.'
  };

  handleChange = (e) => {
    e.preventDefault();
    this.setState({ value: e.target.value });
  }

  handleSubmit = (event, fileType) => {    
    event.preventDefault();

    var data = {
      type: this.state.value,
    }
  
    $('.imageDiv').hide();
    $('.loaderImage').show();

    // Submit form via jQuery/AJAX
    $.ajax({
      type: 'GET',
      url: 'http://httpbin.org/image/' + data.type,
      data: data,
      cache: true,
    })
    .done(function(data) {
      //delay to allow time for spinner to display (otherwise too fast)
      setTimeout(function(){
        $('.imageDiv').show();
        $('.imageDiv').html('<img src=' + this.url + ' />');
        $('.loaderImage').hide();
      }.bind(this), 1500);
    })
    .fail(function(jqXhr) {
      $('.loaderImage').hide();
      $('.imageDiv').show();
      $('.imageDiv').html('<div class="error">The selected option is invalid. Please select an alternate filetype and resubmit.</div>');
    });
  }

  render() {
    return (
      <React.Fragment>
      <Navbar />
      <main className="container">
        <div className="row">
          <div class="col-12">
            <p className="description">{ this.state.description }</p>
          </div>
        </div>

        <form id="imageTypeForm" onSubmit={this.handleSubmit}>
        <div className="row">
          <div class="col-12">
            <ImageSelect handleChange={this.handleChange} imageTypes={this.state.imageTypes} />          
          </div>
        </div>

        <div className="row">
          <div class="col-12">
            <ImageGenerate imageTypes={this.state.imageTypes} />          
          </div>
        </div>

        <div className="row">
          <div class="col-12">

               <button className="btn btn-primary" type="submit">SUBMIT</button>
          </div>
        </div>
        </form>       
      </main>
      </React.Fragment>
    );
  }
}

export default App;
